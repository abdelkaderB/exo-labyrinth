package com.nespresso.sofa.recruitment.labyrinth;

import java.util.HashMap;
import java.util.Map;

public class Labyrinth {

	private final Map<String, Room> rooms;
	private String previeousRoom;
	private String currentRoom;
	private SensorsReporter reporter;

	public Labyrinth(String... gates) {
		rooms = new HashMap<String, Room>();
		reporter = new SensorsReporter();
		for (String gateRepresentation : gates) {
			addRoom(gateRepresentation);
		}
	}

	private void addRoom(String gateRepresentation) {
		final InputParser parser = new InputParser(gateRepresentation);
		final String entrence = parser.getEntrence();
		final Door separator = parser.getSeparator();
		final String exit = parser.getExit();
		if (!rooms.containsKey(entrence))
			rooms.put(entrence, new Room());
		if (!rooms.containsKey(exit))
			rooms.put(exit, new Room());

		rooms.get(entrence).addAdjacentRoom(exit, separator);
		rooms.get(exit).addAdjacentRoom(entrence, separator);
	}

	public void popIn(String room) {
		currentRoom = room;
	}

	public void walkTo(String nextRoom) throws IllegalMoveException {
		if (rooms.containsKey(nextRoom) && rooms.get(currentRoom).canPasseTo(nextRoom)) {
			if (rooms.get(currentRoom).isSensor(nextRoom))
				reporter.readSensor(currentRoom, nextRoom);
			previeousRoom = currentRoom;
			currentRoom = nextRoom;
		} else
			throw new IllegalMoveException();
	}

	public void closeLastDoor() {
		rooms.get(previeousRoom).closeDoorWith(currentRoom);
		rooms.get(currentRoom).closeDoorWith(previeousRoom);
	}

	public String readSensors() {
		return reporter.reporte();
	}

}
