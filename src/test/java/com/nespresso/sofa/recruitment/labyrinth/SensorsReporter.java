package com.nespresso.sofa.recruitment.labyrinth;

public class SensorsReporter {
	private StringBuilder reporteSensores;

	public SensorsReporter() {
		this.reporteSensores = new StringBuilder();
	}

	public void readSensor(String entrance, String exit) {
		reporteSensores.append(entrance+exit);
		reporteSensores.append(";");
		
	}

	public String reporte() {
		StringBuilder reportToPrint=new StringBuilder(reporteSensores);
		return reportToPrint.deleteCharAt(reportToPrint.lastIndexOf(";")).toString();
	}

}
