package com.nespresso.sofa.recruitment.labyrinth;

import java.util.HashMap;
import java.util.Map;

public class Room {
	private final Map<String, Door> adjacentsRooms;

	public Room() {
		this.adjacentsRooms = new HashMap<String, Door>();
	}

	public void addAdjacentRoom(String roomCode, Door door) {
		adjacentsRooms.put(roomCode, door);
	}

	public void closeDoorWith(String currentRoom) {
		if (adjacentsRooms.get(currentRoom).isClosed())
			throw new DoorAlreadyClosedException();
		adjacentsRooms.put(currentRoom,adjacentsRooms.get(currentRoom).close());
	}

	public boolean canPasseTo(String nextRoom) {
		if (adjacentsRooms.containsKey(nextRoom))
			if (adjacentsRooms.get(nextRoom).isClosed())
				throw new ClosedDoorException();
			else
				return true;
		return false;
	}
	
	public boolean isSensor(String room){
		return (adjacentsRooms.get(room)==Door.SENSOR||adjacentsRooms.get(room)==Door.CLOSE_SENSOR);
			 
	}

}
