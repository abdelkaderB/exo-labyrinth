package com.nespresso.sofa.recruitment.labyrinth;

public enum Door {
	SENSOR {
		Door close() {
			return CLOSE_SENSOR;

		}

		boolean isClosed() {
			return false;
		}
	},
	GATE {
		Door close() {
			return CLOSE_GATE;

		}

		boolean isClosed() {
			return false;
		}
	},
	CLOSE_SENSOR {
		Door close() {
			return CLOSE_SENSOR;

		}

		boolean isClosed() {
			return true;
		}
	},
	CLOSE_GATE {
		Door close() {
			return CLOSE_GATE;
		}

		boolean isClosed() {
			return true;
		}
	};
	abstract Door close();

	abstract boolean isClosed();
}
