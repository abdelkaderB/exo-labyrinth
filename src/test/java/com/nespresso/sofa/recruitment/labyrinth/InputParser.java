package com.nespresso.sofa.recruitment.labyrinth;

public class InputParser {

	private final String gateRepresentation;

	public InputParser(String gateRepresentation) {
		this.gateRepresentation = gateRepresentation;
	}

	public Door getSeparator() {
		return getRoomSeparator(gateRepresentation.charAt(1));
	}

	public String getEntrence() {
		return gateRepresentation.charAt(0)+"";
	}

	public String getExit() {
		return gateRepresentation.charAt(2)+"";
	}

	private Door getRoomSeparator(char code) {
		if (code=='|')
			return Door.GATE;
		return  Door.SENSOR;
	}
}
